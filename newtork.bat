@echo off
goto check_Permissions

:check_Permissions
  echo Wyszukiwanie uprawnien...
  whoami /groups | find "S-1-16-12288" > nul
  if %errorLevel% == 0 (
    echo Success: Witaj Administratorze!
    ipconfig /all
    ipconfig /renew
    ipconfig /flushdns
    arp -d
    nbtstat -r
    nbtstat -rr
    netsh winsock reset
    netsh int tcp reset
    netsh int ip reset
    shutdown -r
  ) else (
    echo Failure: Uruchom Jako Administrator!
    pause
  )